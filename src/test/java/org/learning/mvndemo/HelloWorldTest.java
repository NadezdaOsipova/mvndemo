package org.learning.mvndemo;

public class HelloWorldTest {
    public void test1() {
        System.out.println("Running test1");
        HelloWorld helloWorld = new HelloWorld();
        assert "one".equals(helloWorld.do1());
    }
    public void test2() {
        System.out.println("Running test2");
        HelloWorld helloWorld = new HelloWorld();
        assert "two".equals(helloWorld.do2());
    }
    public void test3() {
        System.out.println("Running test3");
        HelloWorld helloWorld = new HelloWorld();
        assert "fourthree".equals(helloWorld.do3());
    }
  // public void test4() {
  //     System.out.println("Running test4");
  //     HelloWorld helloWorld = new HelloWorld();
  //     assert "four".equals(helloWorld.do4());
  // }
}